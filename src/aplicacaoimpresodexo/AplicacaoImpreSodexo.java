package aplicacaoimpresodexo;

import ClassesImpressoes.BorderoComprovanteEntrega;
import ClassesImpressoes.BorderoComprovanteRemissaoCartaSenha;
import ClassesImpressoes.BorderoDeclaracaoEntrega;
import ClassesImpressoes.BorderoDeclaracaoRemissaoCartaSenha;
import ClassesImpressoes.BorderoMestreRemissaoCartaSenha;
import ClassesImpressoes.BorderoMestre_Arquivo_Embossing;
import ClassesImpressoes.CartaBerco;
import ClassesImpressoes.CartaSenha;
import ClassesImpressoes.EMISSAO_HAWB_REMISSAO_CARTA_SENHA;
import ClassesImpressoes.GeradorLog;
import ClassesImpressoes.Gerar_Registro_Por_Lote;
import ClassesImpressoes.SODEXO_ACESSO_INI;
import ClassesImpressoes.SODEXO_ARQUIVO_SENHA_PROCESSAR;
import ClassesImpressoes.SODEXO_EMITIR_CODIGO_BARRA_AUDIT;
import ClassesImpressoes.SODEXO_EMITIR_CODIGO_BARRA_AUDIT_REMISSAO_SENHA;
import ClassesImpressoes.SODEXO_EMITIR_CSV_DWS;
import ClassesImpressoes.SODEXO_EXCLUIR_ARQEMB_ANTIGOS;
import ClassesImpressoes.SODEXO_EXCLUIR_ARQUIVO_CARTA_SENHA;
import ClassesImpressoes.SODEXO_EXCLUIR_ARQUIVO_ICS;
import ClassesImpressoes.SODEXO_EXCLUIR_ARQ_EMB;
import ClassesImpressoes.SODEXO_EXECUTAR_APLICATIVO;
import ClassesImpressoes.SODEXO_EXECUTAR_ConvAlu;
import ClassesImpressoes.SODEXO_EXECUTAR_DECRYPT;
import ClassesImpressoes.SODEXO_FECHAR_PROMPT;
import ClassesImpressoes.SODEXO_IDENTIFICADOR_DE_REGISTROS;
import ClassesImpressoes.SODEXO_MOVER_ARQUIVO_ENTRADA;
import ClassesImpressoes.SODEXO_MOVER_ARQUIVO_ICS;
import ClassesImpressoes.SODEXO_MOVER_ARQUIVO_LOTES_CARTAO;
import ClassesImpressoes.SODEXO_MoverArquivoCSV;
import ClassesImpressoes.SODEXO_MoverArquivoTXT;
import ClassesImpressoes.SODEXO_MoverArquivosPDF;
import ClassesImpressoes.SODEXO_Mover_Arquivo_Descriptografado;
import ClassesImpressoes.SODEXO_Mover_Arquivo_Embossing_Backup;
import ClassesImpressoes.SODEXO_RECEPCIONAR_CHEGADA_ARQUIVO;
import ClassesImpressoes.SODEXO_RELATORIO_INTERNO_CARTAO_PRODUZIDO;
import ClassesImpressoes.SODEXO_RELATORIO_INTERNO_REMISSAO_CARTA_SENHA;
import ClassesImpressoes.SODEXO_VERIFICADOR_DESCRIPTOGRAFIA;
import Test.EMISSAO_HAWB_ARQUIVO_EMBOSSING2;
import com.itextpdf.text.DocumentException;
import java.applet.AudioClip;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class AplicacaoImpreSodexo
{
  public static String processarArquivo = "N";
  private AudioClip somAlerta;
  public static String ArquivoDescricptografado = "NÃO";
  public static int linhaNaoDescriptografada;
  public static String NomedoArquivoEmbossing = null;
  
  public void tocarVida()
  {
    this.somAlerta.play();
  }
  
  public String emitirCartasenha = null;
  
  public static void main(String[] args)
    throws DocumentException, IOException, FileNotFoundException, SQLException, ClassNotFoundException, InterruptedException
  {
    GeradorLog gerador = GeradorLog.getInstance();
    gerador.setLevel(Level.INFO);
    try
    {
      SODEXO_RECEPCIONAR_CHEGADA_ARQUIVO sodexo_recepcionar_chegada_arquivo = new SODEXO_RECEPCIONAR_CHEGADA_ARQUIVO();
      SODEXO_MOVER_ARQUIVO_ENTRADA sodexo_mover_arquivo_entrada = new SODEXO_MOVER_ARQUIVO_ENTRADA();
      SODEXO_IDENTIFICADOR_DE_REGISTROS sodexo_identificador_de_registros = new SODEXO_IDENTIFICADOR_DE_REGISTROS();
      SODEXO_ACESSO_INI ExecSodexo_acesso_ini = new SODEXO_ACESSO_INI();
      SODEXO_FECHAR_PROMPT FinalizarConexoes = new SODEXO_FECHAR_PROMPT();
      SODEXO_EXECUTAR_DECRYPT Decrypt = new SODEXO_EXECUTAR_DECRYPT();
      SODEXO_VERIFICADOR_DESCRIPTOGRAFIA sodexo_verificador_descriptografia = new SODEXO_VERIFICADOR_DESCRIPTOGRAFIA();
      SODEXO_EXECUTAR_ConvAlu ConvAlu = new SODEXO_EXECUTAR_ConvAlu();
      SODEXO_Mover_Arquivo_Descriptografado MoverDescriptografado = new SODEXO_Mover_Arquivo_Descriptografado();
      SODEXO_Mover_Arquivo_Embossing_Backup Mover_Arquivo_Emb_Para_Backup = new SODEXO_Mover_Arquivo_Embossing_Backup();
      


      CartaSenha cartasenha = new CartaSenha();
      CartaBerco berco = new CartaBerco();
      
      BorderoComprovanteEntrega borderocomprovante = new BorderoComprovanteEntrega();
      BorderoDeclaracaoEntrega borderodeclaracao = new BorderoDeclaracaoEntrega();
      SODEXO_EMITIR_CODIGO_BARRA_AUDIT codigodebarraaudit = new SODEXO_EMITIR_CODIGO_BARRA_AUDIT();
      SODEXO_EMITIR_CSV_DWS emitirCsvDws = new SODEXO_EMITIR_CSV_DWS();
      SODEXO_RELATORIO_INTERNO_CARTAO_PRODUZIDO emitirCsvManuseio = new SODEXO_RELATORIO_INTERNO_CARTAO_PRODUZIDO();
      
      SODEXO_EXCLUIR_ARQ_EMB EXCSodexo_excluir_arq_emb = new SODEXO_EXCLUIR_ARQ_EMB();
      SODEXO_EXCLUIR_ARQEMB_ANTIGOS exeSodexo_excluir_arqemb_antigos = new SODEXO_EXCLUIR_ARQEMB_ANTIGOS();
      
      SODEXO_RELATORIO_INTERNO_REMISSAO_CARTA_SENHA GUIA_MANUSEIO_REMISSAO_DE_NOVA_SENHA = new SODEXO_RELATORIO_INTERNO_REMISSAO_CARTA_SENHA();
      SODEXO_ARQUIVO_SENHA_PROCESSAR ProcessarArquivoSenha = new SODEXO_ARQUIVO_SENHA_PROCESSAR();
      
      BorderoComprovanteRemissaoCartaSenha BorderoComprovanteCartaSenhaRemissao = new BorderoComprovanteRemissaoCartaSenha();
      BorderoDeclaracaoRemissaoCartaSenha BorderoDeclaracaoCartaSenhaRemissao = new BorderoDeclaracaoRemissaoCartaSenha();
      BorderoMestreRemissaoCartaSenha BorderoMesteCartaSenhaRemissao = new BorderoMestreRemissaoCartaSenha();
      EMISSAO_HAWB_REMISSAO_CARTA_SENHA EmissaoHawbCartaSenhaRemissao = new EMISSAO_HAWB_REMISSAO_CARTA_SENHA();
      SODEXO_EXCLUIR_ARQUIVO_CARTA_SENHA ExcluirArquivoRemissaoCartaSenha = new SODEXO_EXCLUIR_ARQUIVO_CARTA_SENHA();
      SODEXO_EMITIR_CODIGO_BARRA_AUDIT_REMISSAO_SENHA EMITIR_ARQUIVO_REMISSAO_SENHA_AUDIT = new SODEXO_EMITIR_CODIGO_BARRA_AUDIT_REMISSAO_SENHA();
      BorderoMestre_Arquivo_Embossing BorderoMestreArqEmb = new BorderoMestre_Arquivo_Embossing();
      
      EMISSAO_HAWB_ARQUIVO_EMBOSSING2 EmissaoHawbArquivoEmbossing = new EMISSAO_HAWB_ARQUIVO_EMBOSSING2();
      
      SODEXO_MOVER_ARQUIVO_ICS ConvertidoICS = new SODEXO_MOVER_ARQUIVO_ICS();
      Gerar_Registro_Por_Lote GerarRegistrosPorLote = new Gerar_Registro_Por_Lote();
      SODEXO_EXCLUIR_ARQUIVO_ICS execSodexo_ExcluirArquivo_ICS = new SODEXO_EXCLUIR_ARQUIVO_ICS();
      SODEXO_MOVER_ARQUIVO_LOTES_CARTAO sodexo_mover_arquivo_lotes_cartao = new SODEXO_MOVER_ARQUIVO_LOTES_CARTAO();
      


      SODEXO_MoverArquivosPDF MoverArquivoPDF = new SODEXO_MoverArquivosPDF();
      SODEXO_MoverArquivoTXT MoverArquivoTXT = new SODEXO_MoverArquivoTXT();
      SODEXO_MoverArquivoCSV MoverArquivoCSV = new SODEXO_MoverArquivoCSV();
      SODEXO_EXECUTAR_APLICATIVO ExecutarAplicativoExterno = new SODEXO_EXECUTAR_APLICATIVO();
      



      exeSodexo_excluir_arqemb_antigos.EXCLUIR_ARQ_EMBSetentaDuasHorashoras();
      sodexo_recepcionar_chegada_arquivo.ChegadaDeArquivos();
      sodexo_mover_arquivo_entrada.MoverArquivos_ArquivosRecepcionados();
      sodexo_identificador_de_registros.IdentificadprDeRegistros();
      Mover_Arquivo_Emb_Para_Backup.MoverArquivos_Embossing_Para_Backup();
      if (processarArquivo.equals("S"))
      {
        Decrypt.DescriptografarSenhaEMB();
        sodexo_verificador_descriptografia.VerificarSeOArquivoFoiDescriptografado();
        if (ArquivoDescricptografado.equals("SIM"))
        {
          MoverDescriptografado.MoverArquivos_Descriptografado();
          
          berco.CartaBerco();
          cartasenha.CartaSenha();
          GUIA_MANUSEIO_REMISSAO_DE_NOVA_SENHA.RelatorioInternoCSVManuseio1();
          borderocomprovante.BoderoComprovante();
          borderodeclaracao.BoderoDeclaracao();
          BorderoMestreArqEmb.BorderoMestre_Arquivo_Embossing(args);
          EmissaoHawbArquivoEmbossing.EMISSAO_HAWB_ARQUIVO_EMBOSSING(args);
          codigodebarraaudit.CodigoBarraAudit();
          emitirCsvDws.EmitirCSV_DWS();
          emitirCsvManuseio.RelatorioInternoCSVManuseio();
          
          MoverArquivoCSV.MoverArquivosCSV();
          MoverArquivoPDF.MoverArquivosPDF();
          MoverArquivoTXT.MoverArquivosTXT();
          
          ConvAlu.TratarEmbossingConvAlu();
          
          ConvertidoICS.MoverArquivos_TratadosConvAlu();
          GerarRegistrosPorLote.GerarRegistroPorLote();
          
          EXCSodexo_excluir_arq_emb.EXCLUIR_ARQ_EMB();
          execSodexo_ExcluirArquivo_ICS.EXCLUIRARQUIVO_ICS();
          sodexo_mover_arquivo_lotes_cartao.MoverArquivos_LotesCartao();
          

          GUIA_MANUSEIO_REMISSAO_DE_NOVA_SENHA.RelatorioInternoCSVManuseio1();
          ProcessarArquivoSenha.SODEXO_ARQUIVO_SENHA_PROCESSAR(args);
          

          BorderoComprovanteCartaSenhaRemissao.BorderoComprovanteRemissaoCartaSenhaGeral(args);
          BorderoDeclaracaoCartaSenhaRemissao.BorderoDeclaracaoRemissaoCartaSenhaGeral(args);
          BorderoMesteCartaSenhaRemissao.BorderoMestreRemissaoCartaSenha(args);
          EmissaoHawbCartaSenhaRemissao.EMISSAO_HAWB_REMISSAO_CARTA_SENHA(args);
          EMITIR_ARQUIVO_REMISSAO_SENHA_AUDIT.CodigoBarraAuditRemissaoSenha();
          ExcluirArquivoRemissaoCartaSenha.SODEXO_EXCLUIR_ARQUIVO_CARTA_SENHA(args);
          System.exit(0);
        }
        else if (ArquivoDescricptografado.equals("NÃO"))
        {
          File ArqLog = new File("C:\\Sodexo\\Log\\DtHrRecebimento.txt");
          if (ArqLog.exists())
          {
            String path = "C:\\Sodexo\\Log\\DtHrRecebimento.txt";
            
            File ArquivoLido = new File(path);
            FileOutputStream novaLinha = new FileOutputStream(ArquivoLido, true);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date dataProcessamento = new Date();
            
            String texto1 = "O Arquivo de Embossing não teve suas senhas descriptografadas. Um registro identificado foi o da linha: " + Integer.toString(linhaNaoDescriptografada) + "\t" + "Data e Hora do processo da emissão da mensagem de erro: " + " " + dataProcessamento + "\n";
            novaLinha.write(texto1.getBytes());
            String texto3 = "**********Fim do Processamento do arquivo de Embossing com Erros da descriptografia das Senhas. Arquivo: " + NomedoArquivoEmbossing;
            novaLinha.write(texto3.getBytes());
            novaLinha.close();
          }
          String st = "C:\\Sodexo\\Audio\\alarme.wav";
          InputStream in = new FileInputStream(st);
          AudioStream au = new AudioStream(in);
          AudioPlayer.player.start(au);
          
          JOptionPane.showMessageDialog(null, "Arquivo de Embossing não foi descriptografado suas senhas! Entrar em contato urgente com a equipe de Desenvolvimento. A linha não descriptografada foi a: " + Integer.toString(linhaNaoDescriptografada));
          JOptionPane.showMessageDialog(null, "Dúvidas: Entrar em contato também com o Analista de Sistemas: Fábio Maurício - Techvan (81) 8331-2750");
          AudioPlayer.player.start(au);
        }
        else if (ArquivoDescricptografado.equals("?"))
        {
          System.out.println("Não há arquivo para testar se seuas senhas foram descriptografados");
        }
      }
      else
      {
        File ArqLog = new File("C:\\Sodexo\\Log\\DtHrRecebimento.txt");
        if (ArqLog.exists())
        {
          String path = "C:\\Sodexo\\Log\\DtHrRecebimento.txt";
          
          File ArquivoLido = new File(path);
          FileOutputStream novaLinha = new FileOutputStream(ArquivoLido, true);
          DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
          Date dataProcessamento = new Date();
          
          String texto1 = "O Arquivo de Embossing não poderá ser processado, porque a quantidade de linhas 'REGISTROS' no arquivo difere da quantidade identificada no Trailler do Arquivo. Favor entrar em contato imediatamente com a SODEXO: \tData e Hora do processo da emissão da mensagem de erro:  " + dataProcessamento + "\n";
          novaLinha.write(texto1.getBytes());
          String texto2 = "Dúvidas: Entrar em contato também com o Analista de Sistemas: Fábio Maurício - Techvan (81) 8331-2750\tData e Hora do processo da emissão da mensagem deste aviso:  " + dataProcessamento + "\n";
          novaLinha.write(texto2.getBytes());
          String texto3 = "**********Fim do Processamento com Erros na divergências das quantidades. Arquivo: " + NomedoArquivoEmbossing;
          novaLinha.write(texto3.getBytes());
          novaLinha.close();
        }
        String st = "C:\\Sodexo\\Audio\\alarme.wav";
        InputStream in = new FileInputStream(st);
        AudioStream au = new AudioStream(in);
        AudioPlayer.player.start(au);
        
        JOptionPane.showMessageDialog(null, "Arquivo de Embossing não poderá ser processado, porque a quantidade de linhas 'REGISTROS' no arquivo difere da quantidade identificada no Trailler do Arquivo. Favor entrar em contato imediatamente com a SODEXO!");
        JOptionPane.showMessageDialog(null, "Dúvidas: Entrar em contato também com o Analista de Sistemas: Fábio Maurício - Techvan (81) 8331-2750");
        AudioPlayer.player.start(au);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
