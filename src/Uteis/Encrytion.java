package Uteis;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.andrade
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
 
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import aplicacaoimpresodexo.AplicacaoImpreSodexo; 
public class Encrytion
{    
    
   private KeyGenerator keygenerator;
 private SecretKey myDesKey;
  private Cipher desCipher;
  
  
    public Encrytion() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.keygenerator =  KeyGenerator.getInstance("DES");
        this.myDesKey = keygenerator.generateKey();
        this.desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
    }


     
     public void encrypt(File in, File out) throws IOException, InvalidKeyException {
   desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);
    
    FileInputStream is = new FileInputStream(in);
    CipherOutputStream os = new CipherOutputStream(new FileOutputStream(out), desCipher);
    
    copy(is, os);
    
    os.close();
  }
  
  public void decrypt(File in, File out) throws IOException, InvalidKeyException {
    desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
    
    CipherInputStream is = new CipherInputStream(new FileInputStream(in), desCipher);
    FileOutputStream os = new FileOutputStream(out);
    
    copy(is, os);
    
    is.close();
    os.close();
  }
  
  private void copy(InputStream is, OutputStream os) throws IOException {
    int i;
    byte[] b = new byte[1024];
    while((i=is.read(b))!=-1) {
      os.write(b, 0, i);
    }
       
  } 
       
   
       
    

        //?? test.decrypt(new File("C:\\Sodexo\\Produzidos\\"+AplicacaoImpreSodexo.NomedoArquivoEmbossing),new File("C:\\Sodexo\\Produzidos\\Decrypt\\"+AplicacaoImpreSodexo.NomedoArquivoEmbossing));
  
    
		 
}