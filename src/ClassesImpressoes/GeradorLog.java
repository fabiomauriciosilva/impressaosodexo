/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesImpressoes;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Progamador
 */
public class GeradorLog {
    private static GeradorLog instancia = null;
    private String aspas = "\\\\";
    //private String nomeArquivo = aspas + "192.168.254.233\\ti\\SIGMA\\Clientes\\Log\\logSodexo.txt";
    private String nomeArquivo = aspas + "C:\\SIGMA\\Clientes\\Log\\logSodexo.txt";
    private Level level =  null;
    private Logger logger = null;
    FileHandler fh = null;
    
    private GeradorLog() throws IOException{
      if(level == null)
       level = Level.ALL;
    
     logger = Logger.getLogger("GeradorLog");
     
     try{
       fh = new FileHandler(nomeArquivo,true);
       SimpleFormatter formatter = new SimpleFormatter();
       fh.setFormatter(formatter);
       logger.addHandler(fh);
       logger.setLevel(level);
        } catch (SecurityException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }   
      }

     public void setLevel(Level level){
        instancia.logger.setLevel(level);
      }
     public synchronized static GeradorLog getInstance() throws IOException{
        if (instancia == null){
          instancia = new GeradorLog();
        }
        return instancia;
      }
      public void log(Level level, String mensagem){
        logger.log(level, mensagem);
      }
}
